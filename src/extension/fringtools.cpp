#include <Python.h>

#include <dirent.h>
#include <iostream>
#include <string>
#include <vector>

/*

==============================================================================
This is the original function in python

def _build_tree(self,path):
    l = []
    for fn in os.listdir(path):
        try: p = os.path.join(path, fn)
        except: continue
        s = os.lstat(p)
        if stat.S_ISDIR(s.st_mode):
            l.append( (fn, self._build_tree(p), s.st_size) )
        elif stat.S_ISREG(s.st_mode):
            l.append( (fn, None, s.st_size) )
    return sum_list(os.path.split(path)[-1], l, 0)

==============================================================================

Instead of using SumList, the new build_tree will use a tuple

*/

/// recursive function to build a list system with directory information
PyObject* build_tree( std::string path, const bool& showHidden) {
    PyObject* l = PyList_New(0);
    DIR* d = opendir(path.c_str());
    if(d == 0) Py_RETURN_NONE;

    unsigned long total = 0;
    struct dirent *e;
	while((e = readdir(d)) != NULL) {

        std::string fn = e->d_name;

        if(!showHidden) if(e->d_name[0] == '.') continue;
        if(fn == "." || fn == "..") continue;
		fn = path+"/" +fn;

        struct stat s;
		lstat(fn.c_str(), &s);

		if(S_ISDIR(s.st_mode)) {
            PyObject* sub = build_tree(fn,showHidden);
            PyTuple_SetItem(sub,0,PyString_FromString(e->d_name));
            PyList_Append(l, sub );
            total += PyLong_AsUnsignedLong( PyTuple_GET_ITEM(sub,2) );
		} else if (S_ISREG(s.st_mode)) {
            PyList_Append(l, Py_BuildValue("(sOk)",e->d_name,Py_None,s.st_size) );
            total += s.st_size;
		}

	}
	closedir(d);
    return Py_BuildValue("(sOk)", path.c_str(), l, total );
}

/// initial function call.
static PyObject* fringtools_build_tree(PyObject *self, PyObject *args) {
    const char *path;
    int show_hidden = 1;
    if (!PyArg_ParseTuple(args, "si", &path, &show_hidden)) return NULL;
    return build_tree(std::string(path),(show_hidden==1));
}

//==============================================================================
// module init
//==============================================================================

static PyMethodDef FringtoolsMethods[] = {
    {"build_tree",  fringtools_build_tree, METH_VARARGS, "Walk Tree."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC initfringtools(void) {
    (void) Py_InitModule("fringtools", FringtoolsMethods);
}

int main(int argc, char *argv[]) {
    Py_SetProgramName(argv[0]);
    Py_Initialize();
    initfringtools();
    return 0;
}
