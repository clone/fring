#!/usr/bin/env python

import sys, os

try: import fringtools
except:
    print "Install along with the normal setup.py script before testing"
    sys.exit()

if len(sys.argv) >= 2: tree_path = sys.argv[1]
else: tree_path = os.path.expanduser("~")

def read( t, tab=0 ):
    fn, data, size = t
    print " "*tab,fn,size

    if tab>0: return

    if not data: return
    for e in data:
        read(e,tab+1)

print 'getting "%s":'%tree_path
i = fringtools.build_tree(tree_path, True)
print "done."

read(i)
