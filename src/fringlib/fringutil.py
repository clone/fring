import os
import gtk
import gnomevfs

def get_parent_from_uri(uri):
    """ Get the parent folder from an uri """
    p = os.path.split(uri)
    p = os.path.join(p[:-1])
    return p[0]

def format_uri(uri):
    return gnomevfs.format_uri_for_display(uri)

def pretty_size(size):
    if size >= 1024*1024*1024:
        return "%.1f GiB" % round(size/1024/1024/1024.0)
    elif size >= 1024*1024:
        return "%.1f MiB" % round(size/1024/1024.0)
    elif size >= 1024:
        return "%.1f KiB" % round(size/1024.0)
    else:
        return "%u B" % size

def format_disk_space(size):
    """ Convert bytes to a human readable format.
        Returns a tuple, for example (20,"MiB") """

    if size >= 1024*1024*1024:
        return ("%.1f"%round(size/1024/1024/1024.0),"GiB")
    elif size >= 1024*1024:
        return ("%.1f"%round(size/1024/1024.0),"MiB")
    elif size >= 1024:
        return ("%.1f"%round(size/1024.0),"KiB")
    else:
        return ("%u"%size,"B")

LICENSE = """
fring is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

fring is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with fring; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA.
"""
