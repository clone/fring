import gc

#==================================================================================
class SumList:
    
    def __init__(self, name, children, size):
        self.name = name
        self.children = children
        if self.children is None: self.children = []
        self.size = size

    def append_child(self, sumlist):
        self.children.append(sumlist)
        self.size += sumlist.size

    def has_children(self):
        if len(self.children) > 0: return True
        return False

    def printout(self, tab=0 ):
        print "  "*tab,"%s (%i)"%(sumlist.name,sumlist.size)
        if not sumlist.children: return
        if tab > 1: return
        for e in sumlist.children:
            e.printout(tab+1)

    def sort(self):
        self.children.sort(self.__cmp_fn)
    
    def sort_by_size(self):
        self.children.sort(self.__cmp_by_size)

    def __cmp_by_size(self, b, a ):
        return cmp(a[2], b[2]) # compare sizes

    def __cmp_fn(self, b, a ):
        """ a and b are tuples describing a directory tree. Compare first by directory
            status, then by size, and finally by name. """    

        a_dir = a[1] != None
        b_dir = b[1] != None

        if a_dir and not b_dir:
            return 1
        elif b_dir and not a_dir:
            return -1
        elif a_dir and b_dir:
             return cmp(a[2], b[2]) # compare sizes
        else:
            return cmp(a[0], b[0]) # compare names

    def __getitem__(self, key):
        if key == 0: return self.name
        if key == 1: return self.children
        if key == 2: return self.size

    def __len__(self):
        return 3


#==================================================================================
class SumListCollection:
    """ A repository for folder data.

    Is filled by the walker, and retrieved by the UI class.
    An int is used to track changes in a path.

    Since sumlists are objects, python will store references to them instead
    of copying the data (as it would with tuples). 
    """

    def __init__(self):
        self.clear()

    def set_sumlist(self, uri, sumlist):
        if not isinstance(sumlist,SumList): raise ValueError
        if not (isinstance(uri,unicode) or isinstance(uri,str)): raise ValueError
        if not sumlist.has_children(): return # skip files
        self.collection[uri] = sumlist
        if uri in self.versionControl:
            self.versionControl[uri] += 1
        else:
            self.versionControl[uri] = 0

    def rem_sumlist(self, uri):
        if not (isinstance(uri,unicode) or isinstance(uri,str)): raise ValueError
        if uri in self.collection: del self.collection[uri]
        if uri in self.versionControl: del self.versionControl[uri]
        
    def get_sumlist(self, uri):
        if not (isinstance(uri,unicode) or isinstance(uri,str)): raise ValueError
        if uri in self.collection:
            return self.collection[uri]

    def has_changed(self, uri, version):
        if not (isinstance(uri,unicode) or isinstance(uri,str)): raise ValueError
        if uri in self.versionControl:
            if version != self.versionControl[uri]:
                return True
        return False

    def clear(self):
        self.collection = {}
        self.versionControl = {}
        gc.collect()


