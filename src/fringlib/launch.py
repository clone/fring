#!/usr/bin/env python

# $Id$

# fring is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# fring is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fring; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.

# Copyright Lennart Poettering, Frederic Back 2006

import sys, os
import gtk
import fringui
import gnomevfs

from __init__ import *

if __name__ == "__main__":

    if len(sys.argv) >= 2:
        tree_path = sys.argv[1]
    else:
        tree_path = os.path.expanduser("~")

    # first parameter can be a gnomevfs uri or a local path
    #try: uri = URI(tree_path)
    #except: uri = get_uri_from_local_path(tree_path)

    ui = fringui.UI(tree_path)

    gtk.gdk.threads_init()
    gtk.gdk.threads_enter()
    gtk.main()
    gtk.gdk.threads_leave()
