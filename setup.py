#!/usr/bin/python

from os import sep
from distutils.core import setup, Extension

if __name__ == '__main__' :
    
    setup(\
        name         = "fring",
        version      = "0.1",
        license      = "GPL",
        description  = "Visual disk usage analyser",

        packages     = ['fringlib'],
        package_dir  = {'fringlib': 'src'+sep+'fringlib'},
        scripts      = ['src'+sep+'fring'],

        data_files   = [ 
            ("share"+sep+"fring", ['AUTHORS','COPYING','TODO','README']),
            ("share"+sep+"pixmaps", ["src"+sep+"fring.svg"]),
            ("share"+sep+"applications",['src'+sep+'fring.desktop']),
        ],

        ext_modules = [
            Extension('fringtools',sources = ['src'+sep+'extension'+sep+'fringtools.cpp'])
        ],
    )
